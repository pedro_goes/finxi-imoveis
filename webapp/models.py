# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.gis.db import models
from django.contrib.auth import models as auth_models
from django.utils.translation import ugettext as _

from address.models import AddressField, Address


class Apartment(models.Model):
    thumbnail = models.ImageField(upload_to='apartments', null=True)
    address = AddressField()

    location = models.PointField()

    user = models.ForeignKey(auth_models.User)

    class Meta:
        verbose_name = _('Apartment')
        verbose_name_plural = _('Apartments')