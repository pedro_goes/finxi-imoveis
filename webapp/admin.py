# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.utils.html import format_html
from django.db.models import Case, When
from django.contrib.gis.geos import Point, GEOSGeometry
from django.contrib.admin.views.main import ChangeList

import models
import forms
import controllers


@admin.register(models.Apartment)
class ApartmentAdmin(admin.ModelAdmin):
    actions = None
    list_display = (
        'thumbnail_image', 'address'
    )
    list_display_links = None
    fieldsets = [
        (None, {'fields': ()}),
    ]
    change_list_template = 'apartment/list.html'
    advanced_search_form = forms.AddressSearchForm()

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def thumbnail_image(self, obj):
        if obj.thumbnail:
            return format_html(u'<img src="{src}" height="{height}" width="{width}"></img>'.format(
                src=obj.thumbnail.url,
                height=150,
                width=150
            ))
        return format_html(u'<h4> - Sem thumbnail - </h4>')
    thumbnail_image.short_description = 'Thumbnail'

    def get_changelist(self, request, **kwargs):
        class ActiveChangeList(ChangeList):
            def get_query_set(self, request):
                return super(ActiveChangeList, self).get_query_set(request)

        extra_params = []
        for key in self.advanced_search_form.fields.keys():
            extra_params.append(
                self.other_search_fields.get(key, None)
            )

        if extra_params:
            return ActiveChangeList
        return ChangeList

    def lookup_allowed(self, lookup, value):
        if lookup in self.advanced_search_form.fields.keys():
            return True
        return super(ApartmentAdmin, self).lookup_allowed(lookup, value)

    def changelist_view(self, request, extra_context=None, **kwargs):
        self.other_search_fields = {}
        asf = self.advanced_search_form
        extra_context = { 'asf': asf }

        request.GET._mutable = True

        for key in asf.fields.keys():
            try:
                temp = request.GET.pop(key)
            except KeyError:
                pass
            else:
                if temp != ['']:
                    self.other_search_fields[key] = temp

        request.GET_mutable=False

        # Add extra search form to fix googlemaps render bug
        form = forms.AddressSearchForm()
        extra_context = extra_context or {}
        extra_context.update({
            'form': form,
            'other_search_fields': self.other_search_fields
        })

        return super(ApartmentAdmin, self).changelist_view(request, extra_context=extra_context)

    def get_queryset(self, request):
        qs = super(ApartmentAdmin, self).get_queryset(request)

        if 'address' in self.other_search_fields:
            # Order apartments by distance
            address = self.other_search_fields['address'][0]
            f_lat, f_lng = controllers.get_location_by_googlemaps(address)
            origin = Point(f_lat, f_lng)

            def compare_distance(x, y):
                x_distance = origin.distance(x.location)
                y_distance = origin.distance(y.location)
                return int(x_distance - y_distance)

            def get_queryset_orderby(queryset, pk_list):
                preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(pk_list)])
                return queryset.filter(pk__in=pk_list).order_by(preserved)

            filtered_qs = sorted(qs.all(), cmp=compare_distance)
            filtered_ids = [f.id for f in filtered_qs]

            # TODO: Check why this query didn't work
            # qs = qs.filter(location__distance_lte=(origin, 7000))
            qs = get_queryset_orderby(qs, filtered_ids)

        return qs

