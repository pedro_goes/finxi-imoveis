
from server import settings
import googlemaps
import models
from django.contrib.gis.geos import Point


def get_location_by_googlemaps(address_raw):
    gmaps = googlemaps.Client(key=settings.GOOGLE_API_KEY)

    # Geocoding an address
    geocode_result = gmaps.geocode(address_raw)
    if geocode_result:
        result_location = geocode_result[0]['geometry']['location']
        search_lat = result_location['lat']
        search_lng = result_location['lng']
        return float(search_lat), float(search_lng)


def create_apartment(thumbnail, address, user):
    latitude, longitude = get_location_by_googlemaps(address.raw)

    apartment = models.Apartment.objects.create(
        thumbnail=thumbnail,
        address=address,
        location=Point(latitude, longitude),
        user=user
    )
    return apartment


def read_apartment(id):
    apartment = models.Apartment.objects.get(id=id)
    return apartment


def update_apartment(id, user, thumbnail=None, address=None):
    apartment = read_apartment(id)
    if thumbnail:
        apartment.thumbnail = thumbnail
    if address:
        apartment.address = address
        latitude, longitude = get_location_by_googlemaps(address.raw)
        apartment.location = Point(latitude, longitude)
    apartment.user = user
    apartment.save()
    return apartment


def delete_apartment(id):
    apartment = read_apartment(id)
    apartment.delete()
