# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required

from django.http import Http404

import forms
import models
import controllers


@login_required(login_url='/admin/login/')
def apartment_view(request, id=None):
    try:
        context = {}
        if id:
            apartment = controllers.read_apartment(id=id)
            initial = {
                'address': apartment.address,
                'thumbnail': apartment.thumbnail
            }
            if request.method == 'GET':
                form = forms.ApartmentForm(initial=initial)
                context['apartment'] = apartment
            if request.method == 'POST':
                form = forms.ApartmentForm(request.POST, request.FILES, initial=initial)
                if form.is_valid():
                    apartment = controllers.update_apartment(
                        id=id,
                        thumbnail=request.FILES.get('thumbnail', None),
                        address=form.cleaned_data['address'],
                        user=request.user
                    )
                    return HttpResponseRedirect(
                        reverse('apartment_detail', kwargs={'id': apartment.id}))
        else:
            if request.method == 'GET':
                form = forms.ApartmentForm()
            if request.method == 'POST':
                form = forms.ApartmentForm(request.POST, request.FILES)
                if form.is_valid():
                    apartment = controllers.create_apartment(
                        thumbnail=request.FILES.get('thumbnail', None),
                        address=form.cleaned_data['address'],
                        user=request.user
                    )
                    return HttpResponseRedirect(
                        reverse('apartment_detail', kwargs={'id': apartment.id}))

        context['form'] = form
        return render(request, 'apartment/crud.html', context)

    except models.Apartment.DoesNotExist:
        raise Http404()


def apartment_delete_view(request, id=None):
    try:
        controllers.delete_apartment(id=id)
        return HttpResponseRedirect(reverse('apartment_form'))
    except models.Apartment.DoesNotExist:
        raise Http404()
