from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^apartment/?$', views.apartment_view, name='apartment_form'),
    url(r'^apartment/(?P<id>\w+)/$', views.apartment_view, name='apartment_detail'),
    url(r'^apartment/(?P<id>\w+)/delete/$', views.apartment_delete_view, name='apartment_delete')
]