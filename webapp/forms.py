#!/usr/bin/python
# -*- coding: utf-8 -*-

from django import forms


from address.forms import AddressField


class AddressSearchForm(forms.Form):
    address = AddressField(label=u'Buscar por endereço')
    address_country_code = forms.CharField()
    address_state_code = forms.CharField()
    address_state = forms.CharField()
    address_postal_code = forms.CharField()
    address_longitude = forms.CharField()
    address_locality = forms.CharField()
    address_formatted = forms.CharField()
    address_street_number = forms.CharField()
    address_country = forms.CharField()
    address_latitude = forms.CharField()
    address_route = forms.CharField()


class ApartmentForm(forms.Form):
    thumbnail = forms.ImageField(label='Thumbnail')
    address = AddressField(label='Address')
