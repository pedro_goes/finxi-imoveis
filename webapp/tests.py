# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import re
import random

from django.test import TestCase, Client
from django.core.urlresolvers import reverse

from address.models import Address

from server import settings
import models
import controllers
import populate

# Constants
TEST_FILE_PATH = '{}/media_test/{}'.format(settings.BASE_DIR, 'test.png')
TEST_FILE_PATH_2 = '{}/media_test/{}'.format(settings.BASE_DIR, 'test2.jpeg')
TEST_FILE_PATH_ERROR = '{}/media_test/{}'.format(settings.BASE_DIR, 'test_error.svg')


class ApartmentViewsetTests(TestCase):
    def setUp(self):
        # Execute populate
        populate.populate_db()

    def test_apartment_viewset_form(self):
        """
            This test verifies if user can access the apartment creation form
            returns:
                status_code == 200 and empty a form
        """

        client = Client()
        client.login(username='admin', password='admin123')
        response = client.get(reverse('apartment_form'))
        self.assertEqual(response.status_code, 200)

    def test_apartment_viewset_crud(self):
        """
            This test verifies if user can create, read, update and delete apartment by viewset
        """
        client = Client()
        client.login(username='admin', password='admin123')

        # # Create apartment
        count_before = models.Apartment.objects.all().count()  # Count num of instances before call
        response = client.post(reverse('apartment_form'), {
            'thumbnail': open(TEST_FILE_PATH, 'r'),
            'address': 'Rio de Janeiro - RJ, Brasil',
        })
        self.assertEqual(response.status_code, 302)  # Check POST status
        count_after = models.Apartment.objects.all().count()  # Count num of instances after call
        self.assertEqual(count_after, count_before + 1)  # Check num of instances

        # Get apartment id
        apartment_url = response.url
        apartment_id = re.findall('\d+', apartment_url)[-1]
        expected_apartment = models.Apartment.objects.get(id=apartment_id)

        # # Read apartment
        response = client.get(reverse('apartment_detail', kwargs={'id': apartment_id}))
        self.assertEqual(response.status_code, 200)

        res_address = response.context['apartment']  # Check context
        self.assertEqual(res_address.thumbnail, expected_apartment.thumbnail)
        self.assertEqual(res_address.address.raw, expected_apartment.address.raw)
        self.assertEqual(res_address.location, expected_apartment.location)

        # # Update apartment
        apartment_before = models.Apartment.objects.get(id=apartment_id)
        response = client.post(
            reverse('apartment_detail', kwargs={'id': apartment_id}),
            {
                'thumbnail': open(TEST_FILE_PATH, 'r'),
                'address': 'Minas Gerais - MG, Brasil'
            }
        )
        self.assertEqual(response.status_code, 302)
        # Check instance info
        updated_apartment = models.Apartment.objects.get(id=apartment_id)
        self.assertNotEqual(updated_apartment.thumbnail, apartment_before.thumbnail)
        self.assertNotEqual(updated_apartment.address.raw, apartment_before.address.raw)

        # # Delete apartment
        count_before = models.Apartment.objects.all().count()  # Count num of instances before
        response = client.get(reverse('apartment_delete', kwargs={'id': apartment_id}))
        self.assertEqual(response.status_code, 302)
        count_after = models.Apartment.objects.all().count()  # Check num of instances after
        self.assertEqual(count_after, count_before - 1)

    def test_apartment_viewset_invalid_form(self):
        """
            This test verifies form validation
            returns: status_code == 200
                     error details at context.form.errors
            PS: action need to be interrupted
        """
        client = Client()
        client.login(username='admin', password='admin123')

        # Create apartment with wrong thumbnail
        count_before = models.Apartment.objects.all().count()  # Count num of instances before
        response = client.post(reverse('apartment_form'), {
           'thumbnail': open(TEST_FILE_PATH_ERROR, 'r'),
           'address': 'Rio de Janeiro - RJ, Brasil',
        })
        self.assertEqual(response.status_code, 200)
        # Check response errors
        errors = response.context['form'].errors.keys()
        self.assertIn('thumbnail', errors)
        count_after = models.Apartment.objects.all().count()  # Check num of instances after
        self.assertEqual(count_after, count_before)

        # Create apartment with wrong address
        count_before = models.Apartment.objects.all().count()  # Count num of instances before
        response = client.post(reverse('apartment_form'), {
           'thumbnail': open(TEST_FILE_PATH, 'r'),
           'address': '',
        })
        self.assertEqual(response.status_code, 200)
        # Check response errors
        errors = response.context['form'].errors.keys()
        self.assertIn('address', errors)
        count_after = models.Apartment.objects.all().count()  # Check num of instances after
        self.assertEqual(count_after, count_before)

    def test_apartment_viewset_404_error(self):
        """
            This test verifies error 404 if user access wrong id
            returns: status_code == 404
        """
        client = Client()
        client.login(username='admin', password='admin123')

        # Generate a not existing id
        ids_list = models.Apartment.objects.all().values('id')
        random_id = random.randint(0, 1000)
        while random_id in ids_list:
            random_id = random.randint(0, 1000)

        response = client.get(reverse('apartment_detail', kwargs={'id': random_id}))
        self.assertEqual(response.status_code, 404)
        response = client.post(reverse('apartment_detail', kwargs={'id': random_id}))
        self.assertEqual(response.status_code, 404)
        response = client.get(reverse('apartment_delete', kwargs={'id': random_id}))
        self.assertEqual(response.status_code, 404)

    def test_googlemaps_location_controller(self):
        """
            This test verifies googlemaps latitude longitude result
        """
        search_address = 'Texas, Estados Unidos'
        expected_location = (31.968599, -99.901813)
        expected_margin = 0.00001
        response = controllers.get_location_by_googlemaps(search_address)
        self.assertNotEqual(response, None)
        assert(
            (expected_location[0] - expected_margin) <
            response[0] <
            (expected_location[0] + expected_margin)
        )
        assert(
            (expected_location[1] - expected_margin) <
            response[1] <
            (expected_location[1] + expected_margin)
        )

        search_address = 'Texas, Estados Unidos'
        expected_location = (31.968599, -99.901813)
        response = controllers.get_location_by_googlemaps(search_address)
        self.assertEqual("{0:.4f}".format(response[0]),
                         "{0:.4f}".format(expected_location[0]))
        self.assertEqual("{0:.4f}".format(response[1]),
                         "{0:.4f}".format(expected_location[1]))

    def test_apartment_create_controller(self):
        """
            This test verifies create controller
        """
        from django.contrib.auth import models as auth_models
        user = auth_models.User.objects.all().first()

        # # Create test
        count_before = models.Apartment.objects.all().count()  # Count num of instances before
        # Create apartment
        add = Address(raw='Rio de Janeiro - RJ, Brasil')
        add.save()

        ap = controllers.create_apartment(
            thumbnail='______',
            address=add,
            user=user
        )
        count_after = models.Apartment.objects.all().count()  # Check num of instances after
        self.assertEqual(count_after, count_before + 1)

        # # Read test
        read_ap = controllers.read_apartment(ap.id)
        self.assertEqual(read_ap, ap)

    def test_apartment_update_controller(self):
        from django.contrib.auth import models as auth_models
        user = auth_models.User.objects.all().first()

        # Get random id
        ids_list = [a['id'] for a in models.Apartment.objects.all().values('id')]
        random_idx = random.randint(0, len(ids_list) - 1)
        random_id = ids_list[random_idx]

        # # Update test
        add = Address(raw='São Paulo - SP, Brasil')
        add.save()
        update_ap = controllers.update_apartment(
            id=random_id,
            address=add,
            user=user
        )
        self.assertEqual(update_ap.address.raw, 'São Paulo - SP, Brasil')

    def test_apartment_delete_controller(self):
        # Get random id
        ids_list = [a['id'] for a in models.Apartment.objects.all().values('id')]
        random_idx = random.randint(0, len(ids_list) - 1)
        random_id = ids_list[random_idx]

        # # Delete test
        count_before = models.Apartment.objects.all().count()  # Count num of instances before
        controllers.delete_apartment(random_id)
        result = models.Apartment.objects.filter(id=random_id)
        self.assertEqual(result.count(), 0)
        count_after = models.Apartment.objects.all().count()  # Check num of instances after
        self.assertEqual(count_after, count_before - 1)
