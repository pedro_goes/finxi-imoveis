##
# SOBRE
##

Sistema web de aluguel de imóveis simples desenvolvido com Python 2.7 e Django 1.11.5,
utilizando bibliotecas SQLite3, Pillow, GeoDjango e GoogleMaps.

##
# INSTALAÇÃO
##

# Faça o clone do projeto
| $ hg clone git clone git@bitbucket.org:pedro_goes/finxi-imoveis.git

# Instale as dependências de sistema usando brew:
| $ brew update
| $ brew install sqlite3
| $ brew install spatialite-tools
| $ brew install gdal
| $ brew link gettext --force

# Instale as dependências Python usando pip:
| $ pip install -r requitements.txt

# Instale as dependências JavaScript usando npm:
| $ npm install

# Colete os arquivos estáticos:
| $ python manage.py collectstatic -l

# Construa o banco de dados:
| $ python manage.py migrate
| $ python manage.py makemigrations webapp
| $ python manage.py migrate

##
# CONFIGURAÇÃO
##

# Crie um super usuário para acessar o admin do sistema:
| $ python manage.py createsuperuser
| > username: admin
| > password: admin123

# Configure as traduções
| $ python manage.py makemessages
| $ python manage.py compilemessages

##
# USO
##

# Execute o servidor:
| $ python manage.py runserver

# Execute o populate:
| $ python populate.py

# Acesse os endereços abaixo para:
| Acessar o admin de imóvel - http://127.0.0.1:8000/admin/webapp/apartments
| Solicitar o formulário de imóvel - http://127.0.0.1:8000/webapp/apartments
| Solicitar/Atualizar as informações de um imóvel - http://127.0.0.1:8000/webapp/apartments/{id}
| Remover um imóvel - http://127.0.0.1:8000/webapp/apartments/{id}/delete

##
# TESTE
##

# Para testar o sistema execute:
| $ python manage.py test
