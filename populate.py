#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "server.settings")
import django
django.setup()

from django.contrib.gis.geos import Point
from django.core.files.base import File

from webapp import models, controllers
from server import settings
from address.models import Address
from django.contrib.auth import models as auth_models


def populate_db():
    # Clear database
    auth_models.User.objects.all().delete()
    models.Apartment.objects.all().delete()

    user_1 = auth_models.User.objects.create_superuser('admin', email='finxi1@teste.com', password='admin123')
    user_1.save()
    user_2 = auth_models.User.objects.create_superuser('admin2', email='finxi2@teste.com', password='admin123')
    user_2.save()
    user_3 = auth_models.User.objects.create_superuser('admin3', email='finxi3@teste.com', password='admin123')
    user_3.save()

    add = Address(raw='Rua Honório de Barros, Flamengo, Rio de Janeiro - RJ')
    add.save()
    controllers.create_apartment('test_1', address=add, user=user_1)

    add = Address(raw='Alameda Julieta, Tamboré Santana de Parnaiba - SP')
    add.save()
    controllers.create_apartment('test_2', address=add, user=user_3)

    add = Address(raw='Rua Itumbiara, Cidade Jardim, Goiania - GO')
    add.save()
    controllers.create_apartment('test_3', address=add, user=user_3)

populate_db()
